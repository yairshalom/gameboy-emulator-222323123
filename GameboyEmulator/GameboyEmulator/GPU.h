#pragma once
#include "Interrupts.h"
#include "Constants.h"
#include <iostream>
#include "GUI.h"

enum class Mode : u8 { HBLANK, VBLANK, OAM, TRANSFER_TO_LCD };

using Frame = std::array<std::array<u8, Const::LCD::WIDTH>, Const::LCD::HEIGHT>;

class GPU final : public Singleton<GPU>
{

public:
	
	// Constructors
	explicit GPU(Singleton_t);

	// Methods
	void update(u8 cycles);
	void handleInput();
	void renderFrame();
	void reset();
	std::string getGameFileFromUser();
	void renderHelloScreen();

	// Getters
	static const u8 getCurrentRow() { return _currentRow; };
	static constexpr const bool isMode(const Mode mode) { return _mode == mode; };

	// Operator overloads
	friend std::ostream& operator<<(std::ostream& os, const GPU& gpu);
	friend std::istream& operator>>(std::istream& is, GPU& gpu);

	// Fields
	u32 _clocks;

private:
	

	// Methods
	void drawScanline();
	void setLCDStatus();

	bool isLCDEnabled() const;
	void setMode(const Mode mode);
	u16 getClocksToDrawLine() noexcept;

	u8 getColorType(const u8 colorID, const u8 palette) const;

	void renderTiles(const u8 control);
	void renderSprites(const u8 control);

	void resetScreen();

	static Mode _mode;
	static u8 _currentRow;

	Memory& _memory;
	Interrupts& _interrupts;
	GUI& _gui;

	Frame _frame;
	u16 _scanline;

	bool _shouldUseOGColors;
	bool _firstFrame;
	bool _searchingSpritesFinished;
	bool _increasedLY; // to make sure LY increased only once every CLOCK_TO_DRAW_LINE - 6
};

