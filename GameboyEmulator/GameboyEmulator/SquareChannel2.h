#pragma once
#include "ISquareChannel.h"


class Memory;

class SquareChannel2 final : public ISquareChannel
{
public:
	SquareChannel2(Memory& memory);

	friend std::istream& operator>>(std::istream& is, SquareChannel2& channel2) { return channel2.setState(is); };
	friend std::ostream& operator<<(std::ostream& os, const SquareChannel2& channel2) { return channel2.getState(os); };

private:
	virtual void setEnabled() override;
};

