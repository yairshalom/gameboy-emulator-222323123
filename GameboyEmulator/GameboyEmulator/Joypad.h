#pragma once
#include "mySDL.h"
#include <unordered_map>
#include "Interrupts.h"
#include "Constants.h"


/// <summary>
///
/// The Gameboy's Joypad have 8 buttons:
///		4 directional (up, down, left, right)
///		4 standart buttons (start, select, A, B)
/// 
/// Bit 5 - Button Keys	   (0=selected)
/// Bit 4 - Direction Keys (0=selected)
/// 					   
/// Bit 3 - Down or Start  (0=pressed)
/// Bit 2 - Up or Select   (0=pressed)
/// Bit 1 - Left or B	   (0=pressed)
/// Bit 0 - Right or A	   (0=pressed)
/// 
/// 
/// _joypad variable contains all keys (0-7) in Joypad::id order.
/// 
/// </summary>
class Joypad final : public Singleton<Joypad>
{

public:

	// Constructors
	explicit Joypad(Singleton_t);

	// Methods
	void handleInput();
	u8 getCurrentState() const noexcept;
	void reset();

	// operator overloads
	operator u8() const noexcept;
	friend std::ostream& operator<<(std::ostream& os, const Joypad& joypad);
	friend std::istream& operator>>(std::istream& is, Joypad& joypad);


private:

	// Joypad's Keys
	enum id : u8 
	{
		RIGHT, LEFT, UP, DOWN, // directions
		A, B, SELECT, START    // buttons
	};
	enum class Type : u8
	{
		DIRECTION, BUTTON 
	};

	struct Key 
	{ 
		id id; 
		Type type; 
	};

	const std::unordered_map<SDL_Keycode, Key> KEYS
	{
		{ SDLK_LEFT,   { id::LEFT,  Type::DIRECTION } },
		{ SDLK_RIGHT,  { id::RIGHT, Type::DIRECTION } },
		{ SDLK_UP,     { id::UP,    Type::DIRECTION } },
		{ SDLK_DOWN,   { id::DOWN,  Type::DIRECTION } },
		  
		{ SDLK_s,	    { id::A,	  Type::BUTTON } },
		{ SDLK_a,	    { id::B,	  Type::BUTTON } },
		{ SDLK_LSHIFT, { id::SELECT, Type::BUTTON } },
		{ SDLK_RSHIFT, { id::SELECT, Type::BUTTON } },
		{ SDLK_RETURN, { id::START,  Type::BUTTON } },
	};


	// Methods
	void keyPressed(const Key button);
	void keyReleased(const Key button) noexcept;

	bool isPendingEventKeyPressed() const;

	bool isButtonsRequested() const noexcept;
	bool isDirectionsRequested() const noexcept;

	// Fields 
	const Memory& _memory;
	Interrupts& _interrupts;

	u8 _joypad;
};


