#pragma once
#include "Address.h"
#include "CPU.h"
#include <unordered_map>
#include <queue>

/// <summary>
/// There are 4 Interrupts in the Gameboy
///	Interrupts can be requested and enabled by the program and they are stored
///	in REQUEST_REGISTER and in ENABLED_REGISTER respectively. the first 4 bits
///	in each register represent each interupt - is requested/enabled.
///	Interrupts are only being served if:
///	1. _IME is true
///	2. The interupt is enabled
///	3. There is no pending interupt with heigher priority
/// </summary>


enum class Interrupt : char { NONE = -1, VBLANK, LCD, TIMER, SERIAL, JOYPAD };

class CPU;

class Interrupts final : public Singleton<Interrupts>
{

public:

	// Constructors
	explicit Interrupts(Singleton_t, CPU& CPU);

	static Interrupts& getInstance(CPU* CPU = nullptr)
	{
		static Interrupts i{ Singleton_t(), *CPU };
		return i;
	}

	// Methods
	void doInterrupts();
	void request(const Interrupt& id);
	u8 serve();
	void reset();

	// Getters
	bool isServing() const noexcept { return _servedInterrupt != Interrupt::NONE; };

	// Operator overloads
	friend std::ostream& operator<<(std::ostream& os, const Interrupts& interrupts);
	friend std::istream& operator>>(std::istream& is, Interrupts& interrupts);

	static bool IME; // Interrupt Master Flag

private:

	// interrupt handling behaviour
	enum class Behaviour : u8 
	{ 
		NORMAL, 
		HALT_IME, HALT_NOT_IME_ENABLED_REQUESTS, HALT_NOT_IME_NOT_ENABLED_REQUESTS 
	};

	// Methods
	void setBehaviour(const u8 requests, const u8 enabled);

	// Fields
	const std::unordered_map<Interrupt, u8> ADDRESSES
	{
		{ Interrupt::VBLANK, Address::Int::VBLANK },
		{ Interrupt::LCD,	 Address::Int::LCD	  },
		{ Interrupt::TIMER,  Address::Int::TIMER  },
		{ Interrupt::SERIAL, Address::Int::SERIAL },
		{ Interrupt::JOYPAD, Address::Int::JOYPAD }
	};

	Memory& _memory;
	CPU& _cpu;

	Interrupt _servedInterrupt;
	Behaviour _behaviour;

	u8 _currentCycle;
	bool _wasHalted;
};

