#pragma once
#include <wtypes.h>
#include <string>
#include <array>
#include "Singleton.h"
#include "mySDL.h"
#include "Color.h"
#include "Constants.h"
#include "Menu.h"

using Frame = std::array<std::array<u8, Const::LCD::WIDTH>, Const::LCD::HEIGHT>;

class GUI final : public Singleton<GUI>
{
public:

	// Constructors
	GUI(Singleton_t);
	~GUI() noexcept;

	// methods
	void renderFrame(const Frame& frame);
	void setTitle(const std::string_view title);
	void update();

	void setScale(const size_t& scale);
	SDL_Surface* takeScreenshot() const;
	void setColorsType(const ColorsType& type);
	void checkedForDroppedFiles();
	std::string getGameFileFromUser();
	void renderHelloScreen();
	

	inline size_t getWidth() const noexcept  { return Const::LCD::WIDTH  * _scale; };
	inline size_t getHeight() const noexcept { return Const::LCD::HEIGHT * _scale; };


private:
	// Methods
	bool isThereValidDroppedFile() const;

	// Fields
	Menu* _menu;
	size_t _scale;

	static const std::unordered_map<ColorsType, std::array<Color, 4>> _colors;
	ColorsType _colorsType;

	SDL_Window* _window;
	SDL_Renderer* _renderer;
	SDL_Texture* _texture;


};

