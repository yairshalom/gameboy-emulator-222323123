#pragma once
#include "MBC1.h"

class MBC5 final : public MBC1
{

public:

	// Constructors
	MBC5();
	virtual ~MBC5() noexcept = default;

	// Methods
	virtual bool write(const u16 address, const u8 data) override;
	virtual bool read(const u16 address, u8& data) const override;

	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;

protected:

	// Methods
	virtual void handleBanking(const u16 address, const u8 data) noexcept override;
	virtual void reset();

	// Fields
	std::array<std::array<u8, 0x2000>, 16> _RAMBanks;

private:

	union ROMBank 
	{
		u16 val;
		struct {
			u8 low, high;
		};
	};

	// Fields
	ROMBank _ROMBank;
	u8  _RAMBank;

};

