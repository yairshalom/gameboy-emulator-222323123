#include "Register.h"
#include <map>

/*
	-- add a 16 bits value to this and other register --
	* input: other Register, 16bit value to add
*/
u16 Register::add16(Register& other, const u16 value) noexcept
{
	set16(other, join(other) + value);
	return join(other);
}

/*
	-- subtract 16 bits value from this and other register --
	* input: other Register, 16bit value to subtract
*/
u16 Register::subtract16(Register& other, const u16 value) noexcept
{
	set16(other, join(other) - value);
	return join(other);
}

/*
	-- set this and other Register with 16bit value --
	* input: other Register, 16bit value to set
*/
void Register::set16(Register& other, const u16 value) noexcept
{
	_value = value >> 8;
	other._value = value;
}

/*
	-- join this and other to create 16bit value --
	* input: other Register
	* output: 16bit combined value
*/
u16 Register::join(const Register& other) noexcept
{
	return Utils::join(val(), other);
}
