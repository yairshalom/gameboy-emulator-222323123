#pragma once
#include "MBC1.h"


class MBC2 final : public MBC1
{

public:

	// Constructors
	MBC2();
	virtual ~MBC2() noexcept = default;

	// Methods
	virtual bool write(const u16 address, const u8 data) override;
	virtual bool read(const u16 address, u8& data) const override;

protected:

	// Methods
	virtual void handleBanking(const u16 address, const u8 data) noexcept override;
	virtual void setBank1(const u8 data) noexcept override;
	virtual u8 getROMBank(const u16 address) const noexcept override;

};

