#pragma once
#include "Interrupts.h"


/// <summary>
/// 
/// The Timer in the gameboy has a certain frequency (found in the Timer Controller),
/// requests an interupt when it overflows and set itself to the value of the Timer Modulator.
/// 
/// The Timer Controller is a 3 bit register which controlls the timer.
/// Bit 0-1 specify timer's frequency. 
/// Bit 2 specifies whether the timer is enabled or disabled.
/// 
/// 
/// The Internal Counter is a 16bit register that each loop increases by the current cycles.
/// The Timer is dependet on the Internal Counter, and is only increases if the Internal Counter's
/// specific bit is 0. The bit is TimerCyclesToIncrease >> 1. Each update the timer check if 
/// the Internal Counter's bit is zero, is so increases the Timer and handle if overflow
/// 
/// </summary>
class Timers final : public Singleton<Timers>
{

public:
	
	// Construtors
	explicit Timers(Singleton_t);

	// Methods
	void update(const u8 cycles);
	void reset();

	// Getters
	const bool isCycleAfterOverflow() const noexcept { return _releaseOverflow; };
		
	// operator overloads
	friend std::ostream& operator<<(std::ostream& os, const Timers& timers);
	friend std::istream& operator>>(std::istream& is, Timers& timers);


private:

	const std::array<u32, 4> _FREQUENCIES { 4096, 262144, 65536, 16384 };


	// Methods
	void handleOverflow();
	void releaseOverflow() noexcept;
	void updateTIMA(const u16 internalCounter) noexcept;
	bool isTimerEnabled() const;
	u32 getFrequency() const;


	// Fields
	Memory& _memory;
	Interrupts& _interrupts;

	u8 _lastTimer;
	u8 _lastModulo;

	bool _releaseOverflow;
	bool _overflow;
	bool _isTimerCarry;

	bool _lastSignal;
	bool _lastVisiblePulse;
};

