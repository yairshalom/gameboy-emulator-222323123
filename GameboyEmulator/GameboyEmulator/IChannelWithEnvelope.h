#pragma once
#include "IChannel.h"
#include "VolumeEnvelope.h"

class Memory;


class IChannelWithEnvelope : public IChannel
{
public:
	explicit IChannelWithEnvelope(Memory& memory, u16 NRx0, u16 NRx1, u16 NRx2, u16 NRx3, u16 NRx4);
	virtual void clockEnvelope();
	
	virtual void reset() override;
	virtual void setLengthCounter() override;
	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;
	virtual void setEnvelope();
	virtual void triggerEvent(const u8 frameSequencer);
	virtual void setDacPower(const int value) override;


protected:
	VolumeEnvelope _envelope;

};

