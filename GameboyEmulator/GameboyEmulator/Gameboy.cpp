#include "Gameboy.h"
#include "GameOverException.h"
#include "mySDL.h"
#include <iostream>
#include <fstream>
#include <iomanip>

//? Constructors ?//

/* -- c'tor -- */
Gameboy::Gameboy(Singleton_t) :
	_memory(Memory::getInstance()),
	_cpu(CPU::getInstance()),
	_timers(Timers::getInstance()),
	_gpu(GPU::getInstance()),
	_joypad(Joypad::getInstance()),
	_apu(APU::getInstance())
{
	_gpu.renderHelloScreen();
	loadGame(getGameFileFromUser());
}



//? Methods ?//

/* -- emulate the gameboy emulator -- */
void Gameboy::emulate()
{
	// FramesPerMilliSeconds
	constexpr auto FPMS = 1000 / Const::FPS;

	u32 offset = SDL_GetTicks();
	u32 current = 0;

	while (true)
	{
		_joypad.handleInput();
		_gpu.handleInput();
				
		// validate equal periods of time between updates
		current = SDL_GetTicks();

		if (offset + FPMS < current)
		{
			update();
			offset = current;
		}
	}
}

void Gameboy::reset()
{
	_memory.reset();
	_cpu.reset();
	_gpu.reset();
	_timers.reset();
	_joypad.reset();
	_apu.reset();
}

void Gameboy::loadGame(const std::string_view filePath)
{
	_memory.loadCartridge(filePath);
	reset();
}


void Gameboy::update()
{

	constexpr float MAX_CYCLES_PER_UPDATE = Const::CPS / Const::FPS;
	size_t totalCycles = 0;
	u8 cycles = 0;

	while (totalCycles < MAX_CYCLES_PER_UPDATE)
	{
		// execute opcode	
		cycles = _cpu.execute();
		
		// update units
		_timers.update(cycles);
		_gpu.update(cycles);
		_apu.update(cycles);

		totalCycles += cycles;
	}

	_gpu.renderFrame();

}

std::string Gameboy::getGameFileFromUser()
{
	return _gpu.getGameFileFromUser();
}

std::ostream& operator<<(std::ostream& os, const Gameboy& gameboy)
{
	return os << gameboy._memory
			  << gameboy._cpu    
			  << gameboy._gpu	   
			  << gameboy._timers
			  << gameboy._joypad
			  << gameboy._apu;
}

std::istream& operator>>(std::istream& is, Gameboy& gameboy)
{
	return is >> gameboy._memory
			  >> gameboy._cpu
			  >> gameboy._gpu
			  >> gameboy._timers
			  >> gameboy._joypad
			  >> gameboy._apu;
}
