#include "mySDL.h"
#include "GameOverException.h"
#include <SDL_syswm.h>

std::queue<SDL_Event> SDL::menubarEvents;
std::queue<SDL_Event> SDL::joypadEvents;
std::string SDL::fileDropped;

/* --update all SDL event queues-- */
void SDL::updateEventQueues()
{
  
    for (SDL_Event sdlEvent; SDL_PollEvent(&sdlEvent);)
    {
        switch (sdlEvent.type)
        {
            case SDL_QUIT:
            case SDL_WINDOWEVENT_CLOSE:
                throw GameOverException();

            case SDL_SYSWMEVENT:
            {
                if (sdlEvent.syswm.msg->msg.win.msg == WM_COMMAND)
                    menubarEvents.push(sdlEvent);
                break;
            }
        
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                joypadEvents.push(sdlEvent);
                break;

            case SDL_DROPFILE:
                fileDropped = sdlEvent.drop.file;
                SDL_free(sdlEvent.drop.file);
                break;
        }
    }

}

/* -- waits until a keyboard input received -- */
void SDL::waitForInput()
{
    SDL::updateEventQueues();
   
    for (SDL_Event sdlEvent{0}; sdlEvent.type != SDL_KEYDOWN && sdlEvent.type != SDL_KEYUP;)
        SDL_WaitEvent(&sdlEvent);
}
