#include "IChannel.h"




IChannel::IChannel(Memory& memory, u16 NRx0, u16 NRx1, u16 NRx2, u16 NRx3, u16 NRx4, const LengthCounter& lengthCounter) :
	_memory(memory),
	_NRx0(NRx0),
	_NRx1(NRx1),
	_NRx2(NRx2),
	_NRx3(NRx3),
	_NRx4(NRx4),
	_lengthCounter(lengthCounter),
	_cyclesCount(0)
{
}

void IChannel::reset()
{
	_isRunning = true;
	_isDacOn = true;
	_lengthCounter.setLengthCounter(0, 0);
}

void IChannel::clockLengthCounter()
{
	if (!_lengthCounter.clock()) {
		_isRunning = false;
	}
}

void IChannel::checkObscureLengthClock(const u8 value, const u8 frameSequencer)
{
	if (_lengthCounter.checkObscureLengthClock(value, frameSequencer)) {
		_isRunning = false;
	}
}

void IChannel::triggerEvent()
{
	// if the dac was off before trigger then we disable the channel too
	_isRunning = _isDacOn;
}


void IChannel::setEnabledFlagLengthCounter()
{
	_lengthCounter.setEnabled(
		_memory[_NRx4] & Const::APU::FLAG_CHANNEL_LENGTH_FLAG
	);
}


std::istream& operator>>(std::istream& is, std::shared_ptr<IChannel>& channel)
{
	return channel->setState(is);
}

std::ostream& operator<<(std::ostream& os, const std::shared_ptr<IChannel>& channel)
{
	return channel->getState(os);
}

std::istream& IChannel::setState(std::istream& is)
{
	return is >> _lengthCounter
			  >> _cyclesCount
			  >> _isRunning
			  >> _isDacOn;
}

std::ostream& IChannel::getState(std::ostream& os) const
{
	return os << _lengthCounter << '\n'
			  << _cyclesCount << '\n'
			  << _isRunning << '\n'
			  << _isDacOn << '\n';
}

u16 IChannel::getFrequency()
{
	return _memory[_NRx3] | ((_memory[_NRx4] & Const::APU::FLAG_CHANNEL_FREQUENCY_MSB_FLAG) << 8);
}
