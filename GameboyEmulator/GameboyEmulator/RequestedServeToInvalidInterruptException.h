#pragma once
#include <exception>
#include <string>

class RequestedServeToInvalidInterruptException : public std::exception
{
public:
	RequestedServeToInvalidInterruptException() : 
		std::exception("requested serve to invalid interrupt") {};
};