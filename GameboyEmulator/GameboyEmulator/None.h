#pragma once
#include "IMBC.h"


class None final : public IMBC
{

public:

	// Constructors
	None();
	virtual ~None() noexcept = default;

private:

	// Methods
	virtual bool write(const u16 address, const u8 data) override;
	virtual bool read(const u16 address, u8& data) const override;
	
	virtual std::istream& setState(std::istream& is) { return is; };
	virtual std::ostream& getState(std::ostream& os) const { return os; };
	virtual void reset() {};

};

