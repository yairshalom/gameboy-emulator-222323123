#pragma once

#include <exception>

class InvalidROMBanksCountException : public std::exception
{
public:
	InvalidROMBanksCountException() : std::exception("ROM Banks count not supported") {};
};

class InvalidRAMBanksCountException : public std::exception
{
public:
	InvalidRAMBanksCountException() : std::exception("RAM Banks count not supported") {};
};