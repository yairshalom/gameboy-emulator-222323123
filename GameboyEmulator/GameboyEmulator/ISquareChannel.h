#pragma once
#include "IChannelWithEnvelope.h"

class Memory;

class ISquareChannel : public IChannelWithEnvelope
{
public:
	explicit ISquareChannel(Memory& memory, u16 NRx0, u16 NRx1, u16 NRx2, u16 NRx3, u16 NRx4);
	
	virtual void clock(int cycles) override;
	virtual void triggerEvent(const u8 frameSequencer) override;
	virtual float getSample() override;
	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;
	virtual void reset() override;

	virtual void resetDuty();
	virtual void setDuty();
	virtual void setFrequency();

protected:
	u8 _sampleIndex;
	u8 _selectedDuty;
	u16 _frequency;

};

