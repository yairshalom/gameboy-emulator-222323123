#pragma once
#include "IChannel.h"

class Memory;

class WaveChannel final : public IChannel
{
public:
	explicit WaveChannel(Memory& memory);

	virtual void clock(int cycles) override;
	virtual void setDacPower(const int value) override;
	virtual void setLengthCounter() override;
	virtual void triggerEvent(const u8 frameSequencer);
	virtual float getSample() override;
	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;
	virtual void reset() override;
	virtual void setEnabled() override;


	void setFrequency();
	void readLevel();
	u8 getWaveRam(const u16 address);
	void setWaveRam(const u16 address,const u8 value);

	friend std::istream& operator>>(std::istream& is, WaveChannel& waveChannel);
	friend std::ostream& operator<<(std::ostream& os, const WaveChannel& waveChannel);

private:
	void setWaveByte();

	Const::APU::AudioLevel _outputLevel = Const::APU::AudioLevel::MUTE;
	u16 _sampleIndex = 0;
	u8 _waveByte = 0;
	bool _waveRecentlyRead = true;


};

