#pragma once
#include <exception>
#include <string>

class CannotCreateWindowException : public std::exception
{
public:
	CannotCreateWindowException(const std::string& e) : std::exception(("cannot create window: " + e).c_str()) {};
};